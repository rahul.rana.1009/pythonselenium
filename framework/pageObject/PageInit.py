from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import logging


class PageInit:

    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 12)

    def set_text(self, element, text, element_name):
        try:
            elem = self.wait.until(EC.element_to_be_clickable(element))
            elem.clear()
            elem.send_keys(text)
            logging.info("successfully set the text as " + text + " for " + element_name)
        except TimeoutException:
            logging.error("failure due to Exception")

    def click_element(self, element, element_name):
        try:
            elem = self.wait.until(EC.element_to_be_clickable(element))
            elem.click()
            logging.info("successfully clicked on " + element_name)
        except TimeoutException:
            logging.error("failure due to Exception")
