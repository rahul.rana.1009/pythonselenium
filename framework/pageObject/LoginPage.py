from framework.pageObject.PageInit import PageInit
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class LoginPage(PageInit):
    txt_username = (By.ID, "username")
    txt_password = (By.ID, "password")
    btn_submit = (By.CSS_SELECTOR, "[id='Login'][type='submit']")

    def __init__(self, driver):
        PageInit.__init__(self, driver)

    def enter_user_name(self, username):
        self.set_text(self.txt_username, username, "User Name")

    def enter_password(self, password):
        self.set_text(self.txt_password, password, "Password")

    def click_sign_in(self):
        self.click_element(self.btn_submit, "Button Login")
