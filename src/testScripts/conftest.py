import pytest
from selenium import webdriver


@pytest.fixture(scope="class")
def setup(request):
    driver = webdriver.Chrome(executable_path="../../drivers/chromedriver.exe")
    driver.implicitly_wait(12)
    driver.maximize_window()
    request.cls.driver = driver

    yield driver
    driver.close()
